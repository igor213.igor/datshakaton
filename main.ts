import {imageServices} from "./services/image";
import {paintFactory} from "./painFactory";
import {filterBlackPixels, getPixelData} from "./helpers/image";
import {uiHelpers} from "helpers/ui";
import {balistaHelpers} from "helpers/fire";
import {shoot} from "shoot";

const colorAmount = 1;
const imgWidth = 500;

// BALISTA DATA
const balistaX = imgWidth / 2;
const balistaY = -300;

const setColor = async (x: number, y: number, targetColorsRgb: number[]) => {

    await paintFactory.generateIntervalColorsTest(targetColorsRgb, async (colorKey: string) => {

        const angle = balistaHelpers.calculateAngle(balistaX, balistaY, x, y);
        const distance = balistaHelpers.calculateDistance(balistaX, balistaY, x, y);
        const speed = balistaHelpers.calculateThrowSpeed(distance);
        const force = balistaHelpers.calculateThrowForce(colorAmount, speed);

        const shootResult = await shoot.createShoot(colorKey, angle, force)

        await uiHelpers.sleep(shootResult?.info?.ns / 1000000);

    })
}

const getDataFromImage = () => {
    const url = 'http://s.datsart.dats.team/game/image/85/334.png';

    getPixelData(url)
        .then(async (data) => {
            const filteredData = filterBlackPixels(data);

             for (let index = (filteredData.length - 1); index >= 0; index--) {
           // for (let index = 0; index < filteredData.length; index++) {
                const pixelData = filteredData[index];
                const {x, y, color} = pixelData;

                console.log(x, y, color);

                const colorsArr = Object.values(color);

                await setColor(x, y, colorsArr)
                // if (paintResult?.pikedPaint) {
                //     await uiHelpers.sleep(paintResult.p.info.ns / 1000000);
                // }
                //
            }
        })
        .catch((error) => console.error(error));
}


const main = async () => {
    Promise.all([
        paintFactory.generateIntervalColors(),
        imageServices.getImageMyRgbList(),
        getDataFromImage()
    ])
}

main();








