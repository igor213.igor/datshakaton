// @ts-ignore
import {Canvas, createCanvas, Image, loadImage, ImageData} from 'canvas'

type Color = {
    red: number;
    green: number;
    blue: number;
};

type PixelData = {
    x: number;
    y: number;
    color: Color;
};

async function getImageData(url: string): Promise<ImageData> {
    const image = await loadImage(url);

    const canvas = createCanvas(image.width, image.height)
    const ctx = canvas.getContext("2d");

    if (ctx) {
        ctx.drawImage(image, 0, 0);

        const imageData = ctx.getImageData(0, 0, image.width, image.height);


        return imageData;

    } else {
        throw new Error("Failed to get canvas context")
    }
}

function parseColorData(data: Uint8ClampedArray, index: number): Color {
    return {
        red: data[index],
        green: data[index + 1],
        blue: data[index + 2],
    };
}

async function getPixelData(url: string): Promise<PixelData[]> {
    const imageData = await getImageData(url);

    const pixelData: PixelData[] = [];

    for (let y = 0; y < imageData.height; y++) {
        for (let x = 0; x < imageData.width; x++) {
            const index = (y * imageData.width + x) * 4;
            const color = parseColorData(imageData.data, index);
            pixelData.push({x, y, color});
        }
    }

    return pixelData;
}

function filterBlackPixels(pixelData: PixelData[]): PixelData[] {
    return pixelData.filter(({color}) => {
        return !(color.red === 255 && color.green === 255 && color.blue === 255);
    });
}

export {
    filterBlackPixels,
    getPixelData,
    parseColorData
}
