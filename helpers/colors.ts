// @ts-ignore
interface RGBColor {
    r: number;
    g: number;
    b: number;
}
/**
 * функция, которая принимает:
 * 1. массив всех цветов со склада
 * 2. массив цветов, которые нужны для отрисовки канваса
 * 3. пропорции по которым эти цвета из первого массива могут быть смешаны, чтобы получить  цвета из второго массива*/

function matchOrMixColors(colorCodes: number[], rgbColors: RGBColor[], mixProportions: number[]): number[] {
    const matchedColors: number[] = [];

    function hexToRgb(hex: number): RGBColor {
        return {
            r: (hex >> 16) & 255,
            g: (hex >> 8) & 255,
            b: hex & 255,
        };
    }

    function mixColors(colors: RGBColor[], proportions: number[]): RGBColor {
        const mixedColor: RGBColor = { r: 0, g: 0, b: 0 };

        for (let i = 0; i < colors.length; i++) {
            mixedColor.r += colors[i].r * proportions[i];
            mixedColor.g += colors[i].g * proportions[i];
            mixedColor.b += colors[i].b * proportions[i];
        }

        mixedColor.r = Math.round(mixedColor.r);
        mixedColor.g = Math.round(mixedColor.g);
        mixedColor.b = Math.round(mixedColor.b);

        return mixedColor;
    }

    colorCodes.forEach((code) => {
        const rgbCode = hexToRgb(code);
        const colorCombinations: RGBColor[][] = []; // An array to store different combinations of colors to mix

        // Generate all combinations of rgbColors with the length equal to mixProportions.length
        function generateCombinations(currentCombo: RGBColor[], startIndex: number): void {
            if (currentCombo.length === mixProportions.length) {
                colorCombinations.push(currentCombo);
                return;
            }

            for (let i = startIndex; i < rgbColors.length; i++) {
                generateCombinations([...currentCombo, rgbColors[i]], i + 1);
            }
        }

        generateCombinations([], 0);

        for (let i = 0; i < rgbColors.length; i++) {
            if (rgbCode.r === rgbColors[i].r && rgbCode.g === rgbColors[i].g && rgbCode.b === rgbColors[i].b) {
                matchedColors.push(code);
                break;
            }
        }

        if (!matchedColors.includes(code)) {
            for (let combo of colorCombinations) {
                const mixedColor = mixColors(combo, mixProportions);
                if (rgbCode.r === mixedColor.r && rgbCode.g === mixedColor.g && rgbCode.b === mixedColor.b) {
                    matchedColors.push(code);
                    break;
                }
            }
        }
    });

    return matchedColors;
}

// Example usage:
// const colorCodes = [0xFF5733, 0x1E8449, 0x3498DB];
// const rgbColors: RGBColor[] = [
//     { r: 255, g: 87, b: 51 },
//     { r: 30, g: 132, b: 73 },
//     { r: 52, g: 152, b: 219 },
// ];

// const mixProportions = [0.33, 0.33, 0.34]; // Example mix proportions for three colors

// console.log(matchOrMixColors(colorCodes, rgbColors, mixProportions));

interface RGBColor {
    r: number;
    g: number;
    b: number;
}

function solveLinearSystem(A: number[][], B: number[]): number[] | null {
    const n = A.length;

    for (let i = 0; i < n; i++) {
        // Find the maximum element in the current column
        let maxEl = Math.abs(A[i][i]);
        let maxRow = i;
        for (let k = i + 1; k < n; k++) {
            if (Math.abs(A[k][i]) > maxEl) {
                maxEl = Math.abs(A[k][i]);
                maxRow = k;
            }
        }

        // Swap the maximum row with the current row
        if (i !== maxRow) {
            [A[maxRow], A[i]] = [A[i], A[maxRow]];
            [B[maxRow], B[i]] = [B[i], B[maxRow]];
        }

        // Make all rows below this one have a zero in the current column
        for (let k = i + 1; k < n; k++) {
            const c = -A[k][i] / A[i][i];
            for (let j = i; j < n; j++) {
                if (i === j) {
                    A[k][j] = 0;
                } else {
                    A[k][j] += c * A[i][j];
                }
            }
            B[k] += c * B[i];
        }
    }

    // Solve the upper triangular matrix
    const x = new Array(n).fill(0);
    for (let i = n - 1; i >= 0; i--) {
        let sum = 0;
        for (let j = i + 1; j < n; j++) {
            sum += A[i][j] * x[j];
        }
        x[i] = (B[i] - sum) / A[i][i];
    }

    return x;
}

/**
 * функция, которая принимает цвета из канваса и вычисляет пропорции,
 * чтобы их можно смешать, используя другие цвета*/
function getColorMixProportions(targetColor: RGBColor, baseColors: RGBColor[]): number[] | null {
    if (baseColors.length < 3) {
        throw new Error('At least 3 base colors are required.');
    }

    const A = baseColors.slice(0, 3).map((color) => [color.r, color.g, color.b]);
    const B = [targetColor.r, targetColor.g, targetColor.b];

    const proportions = solveLinearSystem(A, B);

    // If any proportion is less than 0 or greater than 1, the color cannot be produced using the given base colors
    if (proportions?.some((prop) => prop < 0 || prop > 1)) {
        return null;
    }

    return proportions;
}

// Example usage:
// const targetColor = { r: 153, g: 102, b: 204 };
// const baseColors = [
//     { r: 255, g: 0, b: 0 }, // Red
//     { r: 0, g: 255, b: 0 }, // Green
//     { r: 0, g: 0, b: 255 }, // Blue
// ];

// console.log(getColorMixProportions(targetColor, baseColors));