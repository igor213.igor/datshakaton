import {getConfig} from "../config";

const {apiUrl,authToken}=getConfig();

const postData = async (
    url:string,
    data:any,
    options: RequestInit = {}
) => {
    const {headers} = options;

    const response = await fetch(`${apiUrl}${url}`, {
        method: 'POST',
        headers: {
            ...headers,
            'Authorization': `Bearer ${authToken}`
        },
        referrerPolicy: 'origin',
        body: data,
    });

    const result = await response.json();

    if (!response.ok || response.status.toString().startsWith('4')) {
        throw result;
    }

    return result;
};


export const apiHelpers = {
    postData,
}
