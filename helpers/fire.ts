const GRAVITY = 9.80665;
const PI = 3.1415926535898;
const COEF_MASS = 0.001;

const ANGLE = 45 * (PI / 180);
const COS = Math.cos(ANGLE);
const SIN = Math.sin(ANGLE);

// L = 2v₀²sin(α)cos(α) / g.

function calculateDistance(x1: number, y1: number, x2: number, y2: number) {
    return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
}

function calculateAngle(x1: number, y1: number, x2: number, y2: number) {
    const distX = x1 - x2;
    const distY = y1 - y2;
    return (Math.atan(distX / distY) * 180) / PI;
}

function calculateThrowLength(speed: number) {
    const range =
        (2 * (speed * speed) * SIN * COS) / GRAVITY;

    return range;
}

function calculateThrowSpeed(length: number) {
    const speed = Math.sqrt((length * GRAVITY) / SIN / COS / 2);
    return speed;
}

function calculateThrowForce(colorAmount: number, speed: number) {
    return ((COEF_MASS * colorAmount) * speed ** 2) / 2;
}

// // TEST DATA
// const colorAmount = 1;
// const y = 125;
// const x = 125 / 2;
// const imgWidth = 250;

// // BALISTA DATA
// const balistaX = imgWidth / 2;
// const balistaY = -300;

// const angle = calculateAngle(balistaX, balistaY, x, y);
// const distance = calculateDistance(balistaX, balistaY, x, y);
// const speed = calculateThrowSpeed(distance);
// const force = calculateThrowForce(colorAmount, speed);


export const balistaHelpers = {
    calculateThrowLength,
    calculateThrowSpeed,
    calculateThrowForce,
    calculateDistance,
    calculateAngle,
}