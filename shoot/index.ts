import { apiHelpers } from "../helpers/api";

const createShoot = async (colorId: string, angle: number, power: number): Promise<any> => {
    try {
        const formData = new FormData();

        formData.set('angleHorizontal', angle.toString());
        formData.set('angleVertical', "45");
        formData.set('power', power.toString());
        formData.set(`colors[${colorId}]`, "1");

        const result = await apiHelpers.postData('/art/ballista/shoot', formData);

        return result;
    } catch (e) {
        console.error(e)
    }
}

export const shoot = {
    createShoot
}

