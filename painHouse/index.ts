import {apiHelpers} from "../helpers/api";

export type TGetAmountAll = Record<string, number>;

const getAmountAll = async (): Promise<TGetAmountAll | void> => {
    try {
        const result = await apiHelpers.postData('/art/colors/list', {});

        return result.response as TGetAmountAll;
    } catch (e) {
        console.error(e)
    }
}

export const paintHouse = {
    getAmountAll
}

