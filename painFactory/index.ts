import {apiHelpers} from "../helpers/api";
import {uiHelpers} from "../helpers/ui";
import {paintsServices} from "../services/paints";
import {paintHouse, TGetAmountAll} from "../painHouse";

const getGeneratesPaint = async () => {
    try {
        const result = await apiHelpers.postData('/art/factory/generate', {});

        return result;
    } catch (e) {
        console.error(e)
    }
}


const pickPaint = async (colorId: number, tick?: string): Promise<any> => {
    try {
        const formData = new FormData();

        formData.set('num', colorId.toString());

        if (tick) {
            formData.set('tick', tick);
        }

        const result = await apiHelpers.postData('/art/factory/pick', formData);

        return result;
    } catch (e) {
        console.error(e)
    }
}


const generateIntervalColorsFromDb = async (hasInterval?: boolean): Promise<Array<number[]>> => {
    let colorsDbFromArray: Array<number[]> = [];

    const colorsObject = await paintHouse.getAmountAll() as TGetAmountAll;

    colorsDbFromArray = paintsServices.generatedRGBFromDB(colorsObject)

    if (hasInterval) {
        setInterval(async () => {
            const colorsObject = await paintHouse.getAmountAll() as TGetAmountAll;

            colorsDbFromArray = paintsServices.generatedRGBFromDB(colorsObject)
        }, 10000)
    }

    return colorsDbFromArray
}

const generateIntervalColorsMapFromDb = async (hasInterval?: boolean): Promise<Record<string, number[]>> => {
    let colors: Record<string, number[]> = {};

    const colorsObject = await paintHouse.getAmountAll() as TGetAmountAll;

    colors = paintsServices.generatedMapRGBFromDB(colorsObject)

    if (hasInterval) {
        setInterval(async () => {
            const colorsObject = await paintHouse.getAmountAll() as TGetAmountAll;

            colors = paintsServices.generatedMapRGBFromDB(colorsObject)
        }, 10000)
    }

    return colors
}

const generateIntervalColors = async () => {

    let colorsDbFromArray = [
        [252, 194, 27]
    ];

    while (true) {
        const paints = await getGeneratesPaint();

        const paintsResponse = paints?.response;

        if (!paintsResponse) {
            console.error('Не пришел ответ по генерации красок')
            return;
        }
        //Todo const paintsInfo = paints.info;

        for (let i = 1; i <= 3; i++) {
            const rgbColorArray = paintsServices.getRGBfromI(paintsResponse[i]?.color);

            if (paintsServices.hasNeedColor(colorsDbFromArray, rgbColorArray, 20)) {
                console.log(i, ':', rgbColorArray);
                await pickPaint(i)
                console.log('+'.repeat(200));
            }
        }

        await uiHelpers.sleep(700)
    }
}


const generateIntervalColors2 = async () => {
    const paints = await getGeneratesPaint();
    console.log(paints);
    const paintsResponse = paints?.response;
    if (!paintsResponse) {
        console.error('Не пришел ответ по генерации красок')
        return;
    }

    const pikedPaint = await pickPaint(1)
    return {paints, pikedPaint};
}


const generateIntervalColorsTest = async (targetColors: number[], cb: (data: string) => Promise<any>) => {

    let colorsDbFromArray = [
        targetColors
    ];

    const colors = await generateIntervalColorsMapFromDb();

    for (const key of Object.keys(colors)) {
        const color = colors[key];

        if (paintsServices.hasNeedColor(colorsDbFromArray, color, 30)) {
            await cb(key);
            break;
        }
    }
}

export const paintFactory = {
    getGeneratesPaint,
    pickPaint,
    generateIntervalColors,
    generateIntervalColorsFromDb,
    generateIntervalColorsMapFromDb,
    generateIntervalColors2,
    generateIntervalColorsTest
}

