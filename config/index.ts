export interface IAppConfig {
    apiUrl:string;
    authToken:string
    apiFigma:string
    authFigmaToken:string
}

export const getConfig = (): IAppConfig => ({
    apiUrl: process.env.API_URL as string,
    authToken: process.env.AUTH_TOKEN as string,
    apiFigma: process.env.API_FIGMA as string,
    authFigmaToken: process.env.AUTH_FIGMA_TOKEN as string,
})
