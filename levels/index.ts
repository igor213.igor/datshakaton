import {apiHelpers} from "../helpers/api";

const getCurrentInfo = async () => {
    try {
        const result = await apiHelpers.postData('/art/stage/info', {});

        return result.response;
    } catch (e) {
        console.error(e)
    }
}

export const levels = {
    getCurrentInfo
}

