import {levels} from "../levels";
import {Canvas, createCanvas} from 'canvas'
import {paintFactory} from "../painFactory";
import * as fs from "fs";

const getCurrentUrlImageFromApi = async () => {
    const result = await levels.getCurrentInfo();

    return result.canvas.url;
}

const createImageRgbList = async (): Promise<Canvas> => {


    const WIDTH_RGB = 60;
    const HEIGHT_RGB = 60;

    const INITIAL_OFFSET = 70;

    let colorsDb = await paintFactory.generateIntervalColorsMapFromDb();

    const WIDTH = 1200;
    const HEIGHT = 1200;


    const canvas = createCanvas(WIDTH, HEIGHT)
    const ctx = canvas.getContext('2d')

    ctx.drawImage(canvas, 0, 0)

    let offsetXKey = 0;
    let offsetYKey = 0;

    Object.keys(colorsDb).forEach(color => {
        const colorsArr = colorsDb[color]

        const [r, g, b] = colorsArr;

        let offSetX = INITIAL_OFFSET * offsetXKey;
        let offSetY = INITIAL_OFFSET * offsetYKey;

        ctx.fillStyle = `rgb(${r},${g},${b})`

        if ((offSetX + 30) > WIDTH) {
            offsetXKey = 0;
            offsetYKey += 1;


            offSetX = INITIAL_OFFSET * offsetXKey;
            offSetY = INITIAL_OFFSET * offsetYKey;
        }

        ctx.fillRect(offSetX, offSetY, WIDTH_RGB, HEIGHT_RGB)

        ctx.font = "8pt 'PT Sans'";
        ctx.textAlign = "center";
        ctx.fillStyle = "#000";

        ctx.fillText(color, offSetX + 30, offSetY + 30);
        ctx.fillText(colorsArr.toString(), offSetX + 30, offSetY + 30 + 16);

        offsetXKey += 1;
    })

    return canvas;

}

const savedCanvas = (canvas: Canvas) => {
    const buffer = canvas.toBuffer("image/png");

    fs.writeFileSync("./image.png", buffer);
}

const getImageMyRgbList = async (): Promise<void> => {
    const canvasImage = await createImageRgbList();

    savedCanvas(canvasImage)
}


export const imageServices = {
    getCurrentUrlImageFromApi,
    createImageRgbList,
    getImageMyRgbList
}
