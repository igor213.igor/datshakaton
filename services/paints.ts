import {TGetAmountAll} from "../painHouse";

function getRGBfromI(RGBint?: any) {
    if (!RGBint) {
        return []
    }

    const blue = RGBint & 255;
    const green = (RGBint >> 8) & 255;
    const red = (RGBint >> 16) & 255;
    return [red, green, blue];
}

function getIfromRGB(rgb: any) {
    const red = rgb[0];
    const green = rgb[1];
    const blue = rgb[2];
    const RGBint = (red << 16) + (green << 8) + blue;
    return RGBint;
}

const generatedRGBFromDB = (data: TGetAmountAll): Array<number[]> => {
    if (!data) {
        return []
    }

    const arrColors = Object.keys(data);

    const enhanceColors = []

    for (const color of arrColors) {
        const rgbColorsArray = paintsServices.getRGBfromI(color);

        enhanceColors.push(rgbColorsArray)
    }

    return enhanceColors;
}

const generatedMapRGBFromDB = (data: TGetAmountAll): Record<string, number[]> => {
    if (!data) {
        return {}
    }

    const arrColors = Object.keys(data);

    let enhanceColors = {} as Record<string, number[]>

    for (const color of arrColors) {
        const rgbColorsArray = paintsServices.getRGBfromI(color);


        enhanceColors[color] = rgbColorsArray;
    }

    return enhanceColors;

}

const hasNeedColor = (rgbDbList: Array<number[]>, rgb: Array<number>, precision: number) => {

    for (const rgbDb of rgbDbList) {
        const [tr, tg, tb] = rgbDb;
        const [r, g, b] = rgb;

        if (
            Math.abs(r - tr) < precision &&
            Math.abs(g - tg) < precision &&
            Math.abs(b - tb) < precision
        ) {
            return true;
        }
    }

    return false;

}


export const paintsServices = {
    getRGBfromI,
    getIfromRGB,
    generatedRGBFromDB,
    hasNeedColor,
    generatedMapRGBFromDB
}
